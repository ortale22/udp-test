package trak.com.myapplication;

import android.os.StrictMode;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;

import java.io.IOException;
import java.net.DatagramPacket;
import java.net.DatagramSocket;
import java.net.InetAddress;
import java.net.SocketException;
import java.net.SocketTimeoutException;
import java.net.UnknownHostException;
import java.util.Date;
import java.util.Timer;
import java.util.TimerTask;

public class MainActivity extends AppCompatActivity {
    private final String TAG = MainActivity.class.getSimpleName();

    private final int MAX_TIMEOUT = 5000;	// milliseconds
    private final int port = 5343;
    private final String ipAddress = "195.200.142.37";
    private InetAddress local;
    private DatagramSocket dsocket;
    private DatagramPacket packet;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        new Thread(new Runnable() {
            @Override
            public void run() {
                try {
                    local = InetAddress.getByName(ipAddress);
                    dsocket = new DatagramSocket();
                    dsocket.connect(local, port);
                    dsocket.setBroadcast(true);
                    dsocket.setReuseAddress(true);
                } catch (SocketException e) {
                    e.printStackTrace();
                } catch (UnknownHostException e) {
                    e.printStackTrace();
                }

                Timer timerSendPing = new Timer("Timer Send Ping");
                timerSendPing.schedule(timerTaskSendPing, 5000, 5000);
            }
        }).start();

//        try {
//            test1();
//        } catch (IOException e) {
//            e.printStackTrace();
//        }
    }

    final TimerTask timerTaskSendPing = new TimerTask() {
        public void run() {
            sendPing();
        }
    };

    private void sendPing() {
        if (dsocket.isConnected()) {
            String message = "Sending ping";
            send(message);
        }
    }

    private void send(String message) {
        try {
            local = InetAddress.getByName(ipAddress);
            packet = new DatagramPacket(message.getBytes(), message.length(), local, port);
            dsocket.send(packet);

            dsocket.connect(local, port);

            byte[] messageReceive = new byte[128];
            DatagramPacket datagramPacket = new DatagramPacket(messageReceive, messageReceive.length);

            if (dsocket == null) {
                dsocket = new DatagramSocket(port);
            }

            dsocket.setSoTimeout(5000);

            while (true) {
                try {
                    dsocket.receive(datagramPacket);
                    int bytesReceived = dsocket.getReceiveBufferSize();
                    Log.d(TAG, "" + bytesReceived);
                }
                catch (SocketTimeoutException exc) {
                    Log.d(TAG, exc.getMessage());
                    dsocket.close();
                }
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    private void client() throws IOException {
        DatagramSocket clientSocket = new DatagramSocket(port);
        clientSocket.setSoTimeout(5000);
        InetAddress IPAddress = InetAddress.getByName(ipAddress);

        String str = "Hello world!";
        byte[] sendData = str.getBytes();
        //System.out.println("Type Something (q or Q to quit): ");

        DatagramPacket sendPacket = new DatagramPacket(sendData,str.length(), IPAddress, port);
        clientSocket.send(sendPacket);

        byte[] receiveData = new byte[1024];
        DatagramPacket receivePacket = new DatagramPacket(receiveData, receiveData.length);
        clientSocket.receive(receivePacket);

        clientSocket.close();
    }

    private void test1() throws IOException {
        // Port number to access
        // Server to Ping (has to have the PingServer running)
        InetAddress server = InetAddress.getByName(ipAddress);

        // Create a datagram socket for sending and receiving UDP packets
        // through the port specified on the command line.
        DatagramSocket socket = new DatagramSocket(port);

        socket.setReuseAddress(true);
        socket.setBroadcast(true);

        int sequenceNumber = 0;
        // Processing loop.
        while (sequenceNumber < 10) {
            // Timestamp in ms when we send it
            Date now = new Date();
            long msSend = now.getTime();
            // Create string to send, and transfer i to a Byte Array
            // Det timestamp der sættes på er ms siden 1/1-1970
            String str = "PING " + sequenceNumber + " " + msSend + " \n";
            byte[] buf = str.getBytes();
            // Create a datagram packet to send as an UDP packet.
            DatagramPacket ping = new DatagramPacket(buf, buf.length, server, port);

            // Send the Ping datagram to the specified server
            socket.send(ping);
            // Try to receive the packet - but it can fail (timeout)
            try {
                // Set up the timeout 1000 ms = 1 sec
                socket.setSoTimeout(MAX_TIMEOUT);
                // Set up an UPD packet for receiving
                DatagramPacket response = new DatagramPacket(new byte[1024], 1024);
                // Try to receive the response from the ping
                socket.receive(response);
                // If the response is received, the code will continue here, otherwise it will continue in the catch

                // timestamp for when we received the packet
                now = new Date();
                long msReceived = now.getTime();
                // Print the packet and the delay
                //printData(response, msReceived - msSend);
            } catch (IOException e) {
                // Print which packet has timed out
                System.out.println("Timeout for packet " + sequenceNumber);
            }
            // next packet
            sequenceNumber ++;
        }
    }

    private void test2() {

    }
}
